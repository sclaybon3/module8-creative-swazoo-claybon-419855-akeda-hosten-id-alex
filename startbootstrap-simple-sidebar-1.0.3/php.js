// Require the packages we will use:


var http = require('http'),
    socketio = require("socket.io"),
    fs = require('fs');

var app = http.createServer(function(req, res) {
   if(req.url == "/" || req.url == "/phpinfo.php") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end(fs.readFileSync('phpinfo.php')); 
  }else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end("Page Could Not Be Found"); 
  }
	
}).listen(3456);
console.log('Server running at http://localhost:3456/');